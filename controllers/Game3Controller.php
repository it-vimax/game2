<?php
namespace app\controllers;

use app\models\Players;
use Yii;
use yii\web\Controller;

class Game3Controller extends Controller
{
    public function actionBase()
    {
        $res = [];
        $players = Players::find()->asArray()->all();
        $res['players'] = $players;
        return $this->asJson($res);
    }

    public function actionActivePlayer()
    {
        $palyerId = Yii::$app->request->get('playerId');
        $res = [];
        // убираем старую активность пользователя
        $oldActivePlayer = Players::find()->where(['state' => 1])->all()[0];
        $oldActivePlayer->state = 0;
        $oldActivePlayer->save();
        // ставим нового активноо пользователя
        $res['palyerId'] = $palyerId;
        $playerObj = Players::find()->where(['id' => $palyerId])->all()[0];
        $playerObj->state = 1;
        $playerObj->save();
        return Yii::$app->response->statusCode = 200;
    }

    public function actionStep()
    {
        $positionX = Yii::$app->request->get('positionX');
        $positionY = Yii::$app->request->get('positionY');
        $positionZ = Yii::$app->request->get('positionZ');
        $userId = Yii::$app->request->get('userId');
        $res = [];
        $res['request']['positionX'] = $positionX;
        $res['request']['positionZ'] = $positionZ;
        $res['request']['userId'] = $userId;
        $res['response']['collision']['success'] = false;
        $positionUsers = Players::find()->asArray()->all();
        foreach($positionUsers as $positionUser)
        {
            if(     $positionUser['position_x'] == $positionX
                &&  $positionUser['position_y'] == $positionY
                &&  $positionUser['position_z'] == $positionZ)
            {
                $res['response']['collision']['message'] = 'Столкновение с игроком ' . $positionUser['name'] . '!';
                $res['response']['collision']['success'] = true;
                $res['response']['collision']['userName'] = $positionUser['name'];
                break;
            }
        }
        if(!$res['response']['collision']['success'])
        {
            $findThisPlayer = Players::find()->where(['id' => $userId])->all()[0];
            $findThisPlayer->position_x = $positionX;
            $findThisPlayer->position_y = $positionY;
            $findThisPlayer->position_z = $positionZ;
            if($findThisPlayer->save())
            {
                $res['response']['collision']['message'] = 'Перешли';
            }
            else
            {
                $res['response']['collision']['message'] = 'Не получильсь сохранить переход!';
            }

        }
        return $this->asJson($res);
    }
}