<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 19.01.2018
 * Time: 21:59
 */

namespace app\controllers;


use yii\web\Controller;

class GameController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function action2()
    {
        return $this->render('2');
    }

    public function action3()
    {
        return $this->renderAjax('3');
    }

    public function actionControlDiv()
    {
        return $this->renderAjax('control-div');
    }
}