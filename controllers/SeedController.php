<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 18.01.2018
 * Time: 19:08
 */

namespace app\controllers;


use app\models\Players;
use yii\web\Controller;

class SeedController extends Controller
{
    public function actionPlayers()
    {
        $playersArr = [
            [
                'name' => 'Test1',
                'email' => 'test1@blutbad3d.tk',
                'level' => 0,
                'shape' => 0,
                'victories' => 0,
                'loser' => 0,
                'draw' => 0,
                'experience' => 0,
                'valor' => 0,
                'karma' => 0,
                'battle_series' => 0,
                'dinar' => 100,
                'sterling' => 0,
                'financial_transaction' => 50,
                'hp_percent' => 100,
                'hp_start_time' => 0,
                'mp_percent' => 100,
                'mp_start_time' => 0,
                'force' => 3,
                'adroitness' => 3,
                'intuition' => 3,
                'viability' => 3,
                'free_improvements' => 3,
                'helmet' => 0,
                'weapon' => 0,
                'armor' => 0,
                'ring1' => 0,
                'ring2' => 0,
                'ring3' => 0,
                'ring4' => 0,
                'belt' => 0,
                'earring' => 0,
                'necklace' => 0,
                'bracelet' => 0,
                'gloves' => 0,
                'shield' => 0,
                'shoes' => 0,
                'rune1' => 0,
                'rune2' => 0,
                'rune3' => 0,
                'rune4' => 0,
                'rune5' => 0,
                'rune6' => 0,
                'stone1' => 0,
                'stone2' => 0,
                'stone3' => 0,
                'stone4' => 0,
                'stone5' => 0,
                'stone6' => 0,
                'stone7' => 0,
            ],
            [
                'name' => 'Test2',
                'email' => 'test2@blutbad3d.tk',
                'level' => '0',
                'shape' => '0',
                'victories' => '0',
                'loser' => '0',
                'draw' => '0',
                'experience' => '0',
                'valor' => '0',
                'karma' => '0',
                'battle_series' => '0',
                'dinar' => '100',
                'sterling' => '0',
                'financial_transaction' => '50',
                'hp_percent' => '100',
                'hp_start_time' => '0',
                'mp_percent' => '100',
                'mp_start_time' => '0',
                'force' => '3',
                'adroitness' => '3',
                'intuition' => '3',
                'viability' => '3',
                'free_improvements' => '3',
                'helmet' => '0',
                'weapon' => '0',
                'armor' => '0',
                'ring1' => '0',
                'ring2' => '0',
                'ring3' => '0',
                'ring4' => '0',
                'belt' => '0',
                'earring' => '0',
                'necklace' => '0',
                'bracelet' => '0',
                'gloves' => '0',
                'shield' => '0',
                'shoes' => '0',
                'rune1' => '0',
                'rune2' => '0',
                'rune3' => '0',
                'rune4' => '0',
                'rune5' => '0',
                'rune6' => '0',
                'stone1' => '0',
                'stone2' => '0',
                'stone3' => '0',
                'stone4' => '0',
                'stone5' => '0',
                'stone6' => '0',
                'stone7' => '0',
            ],
        ];
        Players::deleteAll();
        foreach($playersArr as $playerArr)
        {
            $player = new Players();
            $player->name = $playerArr['name'];
            $player->email = $playerArr['email'];
            $player->level = $playerArr['level'];
            $player->shape = $playerArr['shape'];
            $player->victories = $playerArr['victories'];
            $player->loser = $playerArr['loser'];
            $player->draw = $playerArr['draw'];
            $player->experience = $playerArr['experience'];
            $player->valor = $playerArr['valor'];
            $player->karma = $playerArr['karma'];
            $player->battle_series = $playerArr['battle_series'];
            $player->dinar = $playerArr['dinar'];
            $player->sterling = $playerArr['sterling'];
            $player->financial_transaction = $playerArr['financial_transaction'];
            $player->force = $playerArr['force'];
            $player->adroitness = $playerArr['adroitness'];
            $player->intuition = $playerArr['intuition'];
            $player->viability = $playerArr['viability'];
            $player->free_improvements = $playerArr['free_improvements'];
            $player->helmet = $playerArr['helmet'];
            $player->weapon = $playerArr['weapon'];
            $player->armor = $playerArr['armor'];
            $player->ring1 = $playerArr['ring1'];
            $player->ring2 = $playerArr['ring2'];
            $player->ring3 = $playerArr['ring3'];
            $player->ring4 = $playerArr['ring4'];
            $player->belt = $playerArr['belt'];
            $player->earring = $playerArr['earring'];
            $player->necklace = $playerArr['necklace'];
            $player->bracelet = $playerArr['bracelet'];
            $player->gloves = $playerArr['gloves'];
            $player->shield = $playerArr['shield'];
            $player->shoes = $playerArr['shoes'];
            $player->rune1 = $playerArr['rune1'];
            $player->rune2 = $playerArr['rune2'];
            $player->rune3 = $playerArr['rune3'];
            $player->rune4 = $playerArr['rune4'];
            $player->rune5 = $playerArr['rune5'];
            $player->rune6 = $playerArr['rune6'];
            $player->stone1 = $playerArr['stone1'];
            $player->stone2 = $playerArr['stone2'];
            $player->stone3 = $playerArr['stone3'];
            $player->stone4 = $playerArr['stone4'];
            $player->stone5 = $playerArr['stone5'];
            $player->stone6 = $playerArr['stone6'];
            $player->stone7 = $playerArr['stone7'];
            $player->save();
        }
        return $this->asJson($playersArr);
    }
}