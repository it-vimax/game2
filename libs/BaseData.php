<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 03.02.2018
 * Time: 16:20
 */

namespace libs;


class BaseData
{
    protected $defaultUrlItems = [];

    function __construct()
    {
        $this->defaultUrlItems['helmet'] = "/char_helmet.gif";
        $this->defaultUrlItems['weapon'] = "/char_weapon.gif";
        $this->defaultUrlItems['armor'] = "/char_armor.gif";
        $this->defaultUrlItems['ring'] = "/char_ring.gif";
        $this->defaultUrlItems['belt'] = "/char_belt.gif";
        $this->defaultUrlItems['earring'] = "/char_earring.gif";
        $this->defaultUrlItems['necklace'] = "/char_necklace.gif";
        $this->defaultUrlItems['bracelet'] = "/char_bracelet.gif";
        $this->defaultUrlItems['gloves'] = "/char_gloves.gif";
        $this->defaultUrlItems['shield'] = "/char_shield.gif";
        $this->defaultUrlItems['shoes'] = "/char_shoes.gif";
        $this->defaultUrlItems['rune'] = "/char_rune.gif";
        $this->defaultUrlItems['stone'] = "/char_stone.gif";
    }
}