<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 03.02.2018
 * Time: 15:45
 */

namespace libs;


use app\models\Players;

class Inventory extends BaseData
{
    private $playerId;

    function __construct($id)
    {
        parent::__construct();
        $this->playerId = $id;
    }

    public function getAll()
    {
        $res = [];
        $res['player'] = Players::find()->where(['id' => $this->playerId])->all()[0];
        $res['defaultUrlItems'] =$this->defaultUrlItems;
        return $res;
    }
}