<?php

use yii\db\Migration;

/**
 * Class m180203_124406_player_table
 */
class m180203_124406_player_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('players', [
            'id' =>$this->primaryKey(),
            'name' => $this->string(), // имя
            'email' => $this->string(), // почта
            'level' => $this->integer(), // уровень
            'shape' => $this->integer(), // образ
            // статистика
            'victories' => $this->integer(), // побед
            'loser' => $this->integer(), // поражений
            'draw' => $this->integer(), // ничьих
            'experience' => $this->integer(), // опыт
            'valor' => $this->integer(), // доблесть
            'karma' => $this->integer(), // карма
            'battle_series' => $this->integer(), // серия поединков
            // финансы
            'dinar' => $this->integer(), // динары
            'sterling' => $this->integer(), // стерлинги
            'financial_transaction' => $this->integer(), // финансовых опираций
            // характеристики
            'hp_percent' => $this->integer(), // процент жизней
            'hp_start_time' => $this->integer(), // жизни время отнятия
            'mp_percent' => $this->integer(), // выносливость процент
            'mp_start_time' => $this->integer(), // время последнего отнятия выносливости
            'force' => $this->integer(), // сила
            'adroitness' => $this->integer(), // ловкость
            'intuition' => $this->integer(), // интуиция
            'viability' => $this->integer(), // жизнеспособность
            'free_improvements' => $this->integer(), // возможных улучшений
            // одет
            'helmet' => $this->integer(), // шлем
            'weapon' => $this->integer(), // оружие
            'armor' => $this->integer(), // броня
            'ring1' => $this->integer(), // кольцо 1
            'ring2' => $this->integer(), // кольцо 2
            'ring3' => $this->integer(), // кольцо 3
            'ring4' => $this->integer(), // кольцо 4
            'belt' => $this->integer(), // пояс
            'earring' => $this->integer(), // серги
            'necklace' => $this->integer(), // ожерелье
            'bracelet' => $this->integer(), // наручьи
            'gloves' => $this->integer(), // перчатки
            'shield' => $this->integer(), // щит
            'shoes' => $this->integer(), // ботинки
            'rune1' => $this->integer(), // рута 1
            'rune2' => $this->integer(), // рута 2
            'rune3' => $this->integer(), // рута 3
            'rune4' => $this->integer(), // рута 4
            'rune5' => $this->integer(), // рута 5
            'rune6' => $this->integer(), // рута 6
            'stone1' => $this->integer(), // камень 1
            'stone2' => $this->integer(), // камень 2
            'stone3' => $this->integer(), // камень 3
            'stone4' => $this->integer(), // камень 4
            'stone5' => $this->integer(), // камень 5
            'stone6' => $this->integer(), // камень 6
            'stone7' => $this->integer(), // камень 7
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180203_124406_player_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180203_124406_player_table cannot be reverted.\n";

        return false;
    }
    */
}
