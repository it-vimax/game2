<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game3players".
 *
 * @property int $id
 * @property string $name
 * @property int $position_x
 * @property int $position_y
 * @property int $position_z
 * @property int $state
 */
class Game3players extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game3players';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_x', 'position_y', 'position_z', 'state'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position_x' => 'Position X',
            'position_y' => 'Position Y',
            'position_z' => 'Position Z',
            'state' => 'State',
        ];
    }
}
