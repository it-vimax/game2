<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "players".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $level
 * @property int $shape
 * @property int $victories
 * @property int $loser
 * @property int $draw
 * @property int $experience
 * @property int $valor
 * @property int $karma
 * @property int $battle_series
 * @property int $dinar
 * @property int $sterling
 * @property int $financial_transaction
 * @property int $hp_percent
 * @property int $hp_start_time
 * @property int $mp_percent
 * @property int $mp_start_time
 * @property int $force
 * @property int $adroitness
 * @property int $intuition
 * @property int $viability
 * @property int $free_improvements
 * @property int $helmet
 * @property int $weapon
 * @property int $armor
 * @property int $ring1
 * @property int $ring2
 * @property int $ring3
 * @property int $ring4
 * @property int $belt
 * @property int $earring
 * @property int $necklace
 * @property int $bracelet
 * @property int $gloves
 * @property int $shield
 * @property int $shoes
 * @property int $rune1
 * @property int $rune2
 * @property int $rune3
 * @property int $rune4
 * @property int $rune5
 * @property int $rune6
 * @property int $stone1
 * @property int $stone2
 * @property int $stone3
 * @property int $stone4
 * @property int $stone5
 * @property int $stone6
 * @property int $stone7
 */
class Players extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'players';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'shape', 'victories', 'loser', 'draw', 'experience', 'valor', 'karma', 'battle_series', 'dinar', 'sterling', 'financial_transaction', 'hp_percent', 'hp_start_time', 'mp_percent', 'mp_start_time', 'force', 'adroitness', 'intuition', 'viability', 'free_improvements', 'helmet', 'weapon', 'armor', 'ring1', 'ring2', 'ring3', 'ring4', 'belt', 'earring', 'necklace', 'bracelet', 'gloves', 'shield', 'shoes', 'rune1', 'rune2', 'rune3', 'rune4', 'rune5', 'rune6', 'stone1', 'stone2', 'stone3', 'stone4', 'stone5', 'stone6', 'stone7'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'level' => 'Level',
            'shape' => 'Shape',
            'victories' => 'Victories',
            'loser' => 'Loser',
            'draw' => 'Draw',
            'experience' => 'Experience',
            'valor' => 'Valor',
            'karma' => 'Karma',
            'battle_series' => 'Battle Series',
            'dinar' => 'Dinar',
            'sterling' => 'Sterling',
            'financial_transaction' => 'Financial Transaction',
            'hp_percent' => 'Hp Percent',
            'hp_start_time' => 'Hp Start Time',
            'mp_percent' => 'Mp Percent',
            'mp_start_time' => 'Mp Start Time',
            'force' => 'Force',
            'adroitness' => 'Adroitness',
            'intuition' => 'Intuition',
            'viability' => 'Viability',
            'free_improvements' => 'Free Improvements',
            'helmet' => 'Helmet',
            'weapon' => 'Weapon',
            'armor' => 'Armor',
            'ring1' => 'Ring1',
            'ring2' => 'Ring2',
            'ring3' => 'Ring3',
            'ring4' => 'Ring4',
            'belt' => 'Belt',
            'earring' => 'Earring',
            'necklace' => 'Necklace',
            'bracelet' => 'Bracelet',
            'gloves' => 'Gloves',
            'shield' => 'Shield',
            'shoes' => 'Shoes',
            'rune1' => 'Rune1',
            'rune2' => 'Rune2',
            'rune3' => 'Rune3',
            'rune4' => 'Rune4',
            'rune5' => 'Rune5',
            'rune6' => 'Rune6',
            'stone1' => 'Stone1',
            'stone2' => 'Stone2',
            'stone3' => 'Stone3',
            'stone4' => 'Stone4',
            'stone5' => 'Stone5',
            'stone6' => 'Stone6',
            'stone7' => 'Stone7',
        ];
    }
}
