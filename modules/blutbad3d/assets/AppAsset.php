<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\blutbad3d\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui-1.8.17.custom.css',
        'css/blutbad3d_main.css',
        'css/blutbad3d_main2.css',
        'css/blutbad3d_baf.css',
        'css/blutbad3d_quest.css',
        'css/blutbad3d_farbtastic.css',
    ];
    public $js = [
        'js/jquery-ui-1.8.17.custom.min.js',
        'js/jquery-ui.js',
        'js/jquery.cookie.js',
        'js/jquery.pnotify.js',
        'js/jquery.placeholder.js',
        'js/jquery.countdown.js',
        'js/jquery.form.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
