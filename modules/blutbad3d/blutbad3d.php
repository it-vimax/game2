<?php

namespace app\modules\blutbad3d;

/**
 * blutbad3d module definition class
 */
class blutbad3d extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\blutbad3d\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->layout = 'main';
        // custom initialization code goes here
    }
}
