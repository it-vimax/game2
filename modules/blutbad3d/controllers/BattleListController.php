<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 03.02.2018
 * Time: 13:49
 */

namespace app\modules\blutbad3d\controllers;


use yii\web\Controller;

class BattleListController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}