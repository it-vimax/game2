<?php

namespace app\modules\blutbad3d\controllers;

use yii\web\Controller;

/**
 * Default controller for the `blutbad3d` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
