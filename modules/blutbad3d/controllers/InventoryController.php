<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 03.02.2018
 * Time: 13:16
 */

namespace app\modules\blutbad3d\controllers;


use app\models\Players;
use libs\Inventory;
use yii\web\Controller;

class InventoryController extends Controller
{
    public function actionIndex()
    {
        $player = Players::find()->all()[0];
        $id = $player->id;

        $inventory = new Inventory($id);
        $allData = $inventory->getAll();
        return $this->render('index', compact('allData'));
    }
}