
<div id="hint1" class="hint"></div>
<table width="100%" style="margin-bottom: 10px;">
    <tbody><tr><td style="width: 1%;">
            <span class="nick">&nbsp;<b class="context name side-0">superhero</b> <span class="level">[0]</span> <a class="info" href="http://damask.blutbad.ru/inf=superhero" target="_blank"> <img alt="" class="sex" src="http://img.blutbad.ru/i/infM.gif" title="Информация о персонаже"></a></span></td><td style="padding-left: 1em;"><div class="hp-pw">
                <div class="hp-tooltip">	    	<div class="hp" name="HP1" width="1" id="HP1" style="background-image: url(http://img.blutbad.ru/i/green1.gif); width: 100%;"><!-- --></div>
                </div>
                <div class="pw-tooltip">	    	<div class="pw" style="background-image: url(http://img.blutbad.ru/i/blue1.gif); width: 100%;"><!-- --></div>
                </div>
            </div></td>
        <td colspan="4" align="right">
            <input class="xbbutton" type="submit" value="Подсказка" onclick="window.open('http://enc.blutbad.ru/oldhelp/zayavka.html', 'help', 'height=450,width=500,location=no,menubar=no,status=no,toolbar=no,scrollbars=yes')">
            <input class="xgbutton" type="submit" value="Вернуться" onclick="location.href='main.php?top=0.467837356797105';">
        </td></tr>
    </tbody></table>

<table class="table-but-type">
    <tbody><tr>
        <td class="table-but-lab">Бои:</td>
        <td class="table-but">
            <div>
                <input id="zbtren" class="zay_btn xbsel xbbutton" type="button" value="Тренировочные" data-type-num="tren">
                <input id="zbcur" class="zay_btn xgbutton" type="button" value="Текущие" data-type-num="tklogs">
                <input id="zbend" class="zay_btn xgbutton" type="button" value="Завершенные" data-type-num="end">
                <div class="zaybutload" id="b_lock"></div>
            </div>
        </td>
    </tr>
    </tbody></table>

<table width="100%" cellspacing="0" cellpadding="0">
    <tbody><tr>
        <td valign="top">


            <script>
                $(function() {
                    $( ".zay_btn" ).click(function() {
                        var s_id = $( this ).attr('id');
                        var tmp_val = $( this ).val();

                        $( ".table-but input" ).removeClass( "xbsel xbbutton" ).addClass( "xgbutton" );
                        $( this ).removeClass( "xgbutton" ).addClass( "xbsel xbbutton" );

                        if ($( this ).hasClass( "xbbutton" )) {
                            $( ".table-but input" ).prop('disabled', true);
                            $( "#b_lock" ).show();
                            $( "#b_lock" ).css("left", $( this ).offset().left)
                            $( "#b_lock" ).css("top", $( this ).offset().top)

                            $( this ).val( " " );
                        }

                        $( "#zaybody" ).animate({opacity: 0}, 200 );

                        $.post(file_zay, {
                            cmd: 'getpage',
                            c_page: s_id,
                            nd: nd,
                            user_id: user_id,
                            user_hach: user_hach,
                            is_ajax: 1
                        }, function(data) {

                            $( "#zaybody" ).html( data );
                            $( "#zaybody" ).animate( {opacity: 1}, 200 );
                            $( "#b_lock" ).hide();
                            $( "#" + s_id ).val( 'ждем' );

                            setTimeout(function() {
                                $( ".table-but input" ).prop("disabled", false);
                                $( "#" + s_id ).val( tmp_val );
                            }, 1000);

                            $('.nick-col a.more').click(function() {
                                var $this = $(this);
                                var list = $this.parent().parent();
                                if ($this.text() == '(свернуть)') {
                                    list.removeClass('show-all-' + this.rel);
                                    $(this).text('... (развернуть)');
                                } else {
                                    list.addClass('show-all-' + this.rel);
                                    $(this).text('(свернуть)');
                                }
                                return false;
                            });

                            $('.table-list .timeout').each(function() {
                                var $this = $(this);
                                var until = Number($this.attr('data'));

                                if (until > 0 && (s_id == 'zbgroup' || s_id == 'zbhaot')) {
                                    $this.countdown({
                                        compact: true,
                                        format: 'MS',
                                        onExpiry: function(){
                                            setTimeout(function() {
                                                location.href = reloadLink;
                                            }, 5000);
                                        },
                                        onTick: function(periods){
                                            var seconds = periods[0] * 31557600 + periods[1] * 2629800 + periods[2] * 604800 + periods[3] * 86400 + periods[4] * 3600 + periods[5] * 60 + periods[6];
                                            if (seconds < 60) {
                                                $this.addClass('attention');
                                            }
                                        },
                                        until: until
                                    });
                                }
                            });


                        });
                    });
                });
            </script>

            <div id="inv-overlay">&nbsp;</div>
            <div id="zaybody">
                <div class="battleground battleground-training">
                    <table cellspacing="0" cellpadding="0"><tbody><tr><td>
                                <form action="zayavka.php" class="clearfix" method="get" name="form-tren" id="form-tren">
                                    <fieldset><legend>
                                            <b>Подать заявку на бой</b> </legend>
                                        Таймаут <select name="timeout">
                                            <option value="3">3 мин.</option>
                                            <option value="4">4 мин.</option>
                                            <option value="5">5 мин.</option>
                                            <option value="7">7 мин.</option>
                                            <option value="10" selected="">10 мин.</option>
                                        </select> Тип боя
                                        <select name="k">
                                            <option value="1">обычный</option>
                                        </select>
                                        <input name="cmd" type="hidden" value="tren.create">
                                        <input name="level" type="hidden" value="tren">
                                        <input type="hidden" name="zay_section" value="tren">
                                        <input class="xbbutton" type="submit" value="Подать заявку">
                                        &nbsp;
                                    </fieldset>
                                </form>
                            </td>

                            <td style="padding-left: 10px;">
                                <fieldset style="text-align: center;" id="battle-training">
                                    <legend>Тренировочный бой</legend>
                                    <form action="zayavka.php" class="clearfix" method="get" name="form-tren-tr" id="form-tren-tr">
                                        <input type="hidden" name="level" value="tren">
                                        <input type="hidden" name="zay_section" value="tren">

                                        <input type="hidden" name="trainstart" value="1">
                                        <input type="hidden" name="duel_shadow" value="1">
                                        <input class="xgbutton" type="submit" value="Начать">
                                    </form>
                                </fieldset>
                            </td>

                        </tr>
                        <tr><td><br>
                            </td></tr></tbody></table>
                </div>     </div>

        </td></tr></tbody></table><div id="tooltip" style="display: none; left: 213px; right: auto; top: 38px;"><h3 style="display: none;"></h3><div class="body"><span class="nowrap">Уровень выносливости: 18/18</span></div><div class="url" style="display: none;"></div></div>