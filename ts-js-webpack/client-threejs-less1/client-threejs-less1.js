var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
renderer.setSize(800, 800);
$('.three-test').append(renderer.domElement);
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(45, 800 / 800, 1, 10000);
camera.position.y = -800;
camera.position.z = 800;
camera.rotation.x = 0.70;
var plane = new THREE.Mesh(new THREE.PlaneGeometry(500, 500, 1), new THREE.MeshNormalMaterial());
var cube = new THREE.Mesh(new THREE.BoxGeometry(60, 60, 600), new THREE.MeshNormalMaterial());
cube.position.z = 100;
cube.position.x = -280;
cube.position.y = 200;
var cube2 = new THREE.Mesh(new THREE.BoxGeometry(60, 60, 600), new THREE.MeshNormalMaterial());
cube2.position.z = 100;
cube2.position.x = 280;
cube2.position.y = 200;
var sphere = new THREE.Mesh(new THREE.SphereGeometry(100, 100, 5), new THREE.MeshNormalMaterial());
sphere.position.z = 50;
sphere.position.x = 0;
sphere.position.y = 0;
scene.add(plane);
scene.add(cube);
scene.add(cube2);
scene.add(sphere);
renderer.render(scene, camera);
$('body').on('click', '#plus', function () {
    ajaxPossition('+');
});
$('body').on('click', '#minus', function () {
    ajaxPossition('-');
});
function ajaxPossition(bechavir) {
    switch (bechavir) {
        case '+':
            sphere.position.x += 10;
            break;
        case '-':
            sphere.position.x -= 10;
            break;
    }
    renderer.render(scene, camera);
    console.log(sphere.position.x);
    $.ajax({
        url: '/chat/position',
        //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
        //dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
        data: { position: sphere.position.x },
        async: true,
        cache: true,
        contentType: "application/x-www-form-urlencoded",
        type: "GET",
        success: function (data) {
            if (!data) {
                console.warn('false');
                return;
            }
        },
        error: function (error) {
            console.log(error);
        },
        beforeSend: function () { },
        complete: function () { } //	срабатывает по окончанию запроса
    });
}
var socket = io(':6001');
console.log('client ready');
socket.on('private-position:go', function (data) {
    appendMessage(data);
});
function appendMessage(data) {
    console.log('ответ по соккету');
    console.log(data);
    sphere.position.x = Number(data.position);
    renderer.render(scene, camera);
}
//# sourceMappingURL=client-threejs-less1.js.map