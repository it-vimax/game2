"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _SceneGame = require("./libs/SceneGame");
var SceneGame = _SceneGame.SceneGame;
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        SceneGame.initSceneGame();
    };
    return Main;
}());
$(document).ready(function () {
    Main.main();
});
//# sourceMappingURL=game_3.js.map