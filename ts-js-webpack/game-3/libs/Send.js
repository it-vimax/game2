"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Ajax = (function () {
    function Ajax() {
        this.GET = 'get';
        this.POST = 'post';
    }
    Ajax.prototype.activePlayer = function (url, palyerId, callback) {
        this.data = {
            playerId: palyerId
        };
        this.url = url;
        this.method = this.GET;
        Ajax.callback = callback;
        this.ajax();
    };
    Ajax.prototype.get = function (url, data, callback, positionClick) {
        this.url = url;
        this.data = data;
        this.method = this.GET;
        Ajax.callback = callback;
        Ajax.positionClick = positionClick;
        this.ajax();
    };
    Ajax.prototype.basePosition = function (url, callback) {
        this.url = url;
        this.data = {};
        this.method = this.GET;
        Ajax.callback = callback;
        this.ajax();
    };
    Ajax.response = function (data) {
        eval(Ajax.callback + '(data, Ajax.positionClick)');
    };
    Ajax.prototype.ajax = function () {
        $.ajax({
            url: this.url,
            //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            dataType: "json",
            data: this.data,
            async: true,
            cache: true,
            contentType: "application/x-www-form-urlencoded",
            type: this.method,
            success: function (data) {
                if (!data) {
                    console.warn('false');
                    return;
                }
                Ajax.response(data);
            },
            error: function (error) {
                console.log(error);
            },
            beforeSend: function () { },
            complete: function () { } //	срабатывает по окончанию запроса
        });
    };
    return Ajax;
}());
exports.Ajax = Ajax;
//# sourceMappingURL=Send.js.map