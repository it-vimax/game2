"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseObj = (function () {
    function BaseObj() {
    }
    BaseObj.prototype.setGeometry = function (geometry) {
        this.geometry = geometry;
    };
    BaseObj.prototype.setMaterial = function (material) {
        this.material = material;
    };
    BaseObj.prototype.mesh = function () {
        this.obj = new THREE.Mesh(this.geometry, this.material);
    };
    return BaseObj;
}());
exports.BaseObj = BaseObj;
//# sourceMappingURL=BaseObj.js.map