"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _GameColors = require("./GameColors");
var GameColors = _GameColors.GameColors;
var Field = (function () {
    function Field() {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "Field";
        var geom = new THREE.BoxGeometry(100, 100, 10);
        var mat = new THREE.MeshPhongMaterial({
            color: GameColors.blue
        });
        var m = new THREE.Mesh(geom, mat);
        m.position.x = 100;
        m.position.y = 10;
        m.position.z = 10;
        m.castShadow = true;
        m.receiveShadow = true;
        this.mesh.add(m);
        var geom2 = new THREE.BoxGeometry(100, 10, 10);
        var mat2 = new THREE.MeshPhongMaterial({
            color: GameColors.red
        });
        var m2 = new THREE.Mesh(geom2, mat2);
        m2.position.x = 0;
        m2.position.y = 0;
        m2.position.z = 0;
        m2.castShadow = true;
        m2.receiveShadow = true;
        this.mesh.add(m2);
    }
    return Field;
}());
exports.Field = Field;
//# sourceMappingURL=Field.js.map