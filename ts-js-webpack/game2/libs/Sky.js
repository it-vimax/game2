"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Cloud = require("./Cloud");
var Cloud = _Cloud.Cloud;
var Sky = (function () {
    function Sky() {
        this.mesh = new THREE.Object3D();
        this.nClouds = 20;
        this.clouds = [];
        var stepAngle = Math.PI * 2 / this.nClouds;
        for (var i = 0; i < this.nClouds; i++) {
            var c = new Cloud();
            this.clouds.push(c);
            var a = stepAngle * i;
            var h = 750 + Math.random() * 200;
            c.mesh.position.y = Math.sin(a) * h;
            c.mesh.position.x = Math.cos(a) * h;
            c.mesh.position.z = -400 - Math.random() * 400;
            c.mesh.rotation.z = a + Math.PI / 2;
            var s = 1 + Math.random() * 2;
            c.mesh.scale.set(s, s, s);
            this.mesh.add(c.mesh);
        }
    }
    return Sky;
}());
exports.Sky = Sky;
//# sourceMappingURL=Sky.js.map