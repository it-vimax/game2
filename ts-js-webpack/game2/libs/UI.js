var UI;
(function (UI_1) {
    var SceneGame = MyGame.SceneGame;
    var UI = (function () {
        function UI() {
        }
        UI.prototype.showPlayerMenu = function (playerArr, activePlayer) {
            var html = "";
            html += "<option value='" + activePlayer.id + "' selected>" + activePlayer.name + "</option>";
            for (var _i = 0, playerArr_1 = playerArr; _i < playerArr_1.length; _i++) {
                var player = playerArr_1[_i];
                html += "<option value='" + player.playerId + "'>" + player.playerName + "</option>";
            }
            $('#myPlayer').text(activePlayer.name);
            $('#selectPlayer').html(html);
        };
        UI.prototype.onSelectPlayerOption = function () {
            $('#selectPlayer').on('click', 'option', function (e) {
                var playerId = $(e.target).attr('value');
                SceneGame.newActivePlayer(playerId);
            });
        };
        UI.prototype.positionMouse = function (x, y) {
            $("#menu .mousePositionX").text(x);
            $("#menu .mousePositionY").text(y);
        };
        UI.prototype.positionBox = function (x, y, z) {
            $("#menu .boxPositionX").text(x);
            $("#menu .boxPositionY").text(y);
            $("#menu .boxPositionZ").text(z);
        };
        UI.prototype.divideScalar = function (x, y, z) {
            $("#menu .divideScalarX").text(x);
            $("#menu .divideScalarY").text(y);
            $("#menu .divideScalarZ").text(z);
        };
        UI.prototype.positionFloor = function (x, y, z) {
            $("#menu .floorX").text(x);
            $("#menu .floorY").text(y);
            $("#menu .floorZ").text(z);
        };
        UI.prototype.positionMultiplyScalar = function (x, y, z) {
            $("#menu .multiplyScalarX").text(x);
            $("#menu .multiplyScalarY").text(y);
            $("#menu .multiplyScalarZ").text(z);
        };
        UI.prototype.positionAddScalar = function (x, y, z) {
            $("#menu .addScalarX").text(x);
            $("#menu .addScalarY").text(y);
            $("#menu .addScalarZ").text(z);
        };
        return UI;
    }());
    UI_1.UI = UI;
})(UI || (UI = {}));
//# sourceMappingURL=UI.js.map