"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maksim on 31.01.2018.
 */
var Box = (function () {
    function Box() {
    }
    //    public constructor(){}
    Box.prototype.setSize = function (x, y) {
        if (typeof y != 'number') {
            y = 0;
        }
        if (typeof x != 'number') {
            x = 0;
        }
        this.x = x;
        this.y = y;
    };
    Box.prototype.go = function (x, y) {
        this.x += x;
        this.y += y;
    };
    Box.prototype.stringToArr = function (value) {
        if (typeof value !== 'string') {
            value = '';
        }
        var res = value.split(', ');
        return res;
    };
    return Box;
}());
exports.Box = Box;
//# sourceMappingURL=Box.js.map