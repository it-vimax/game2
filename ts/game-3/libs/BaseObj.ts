module ObjectScene
{
    export class BaseObj
    {
        public obj:any;
        public geometry:any;
        public material:any;

        public setGeometry(geometry:any):void
        {
            this.geometry = geometry;
        }

        public setMaterial(material:any):void
        {
            this.material = material;
        }

        public mesh():void
        {
            this.obj = new THREE.Mesh(this.geometry, this.material);
        }
    }
}