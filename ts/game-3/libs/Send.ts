module Send
{
    export class Ajax
    {
        private GET: string = 'get';
        private POST: string = 'post';
        private url: string;
        private data: any;
        private method: string;
        private static callback: string;
        private static obj: string;
        private static positionClick: any;

        public activePlayer(url:string, palyerId:number, callback:string):void
        {
            this.data = {
                playerId: palyerId
            };
            this.url = url;
            this.method = this.GET;
            Ajax.callback = callback;
            this.ajax();
        }

        public get(url:string, data:any, callback:string, positionClick:any):void
        {
            this.url = url;
            this.data = data;
            this.method = this.GET;
            Ajax.callback = callback;
            Ajax.positionClick = positionClick;
            this.ajax();
        }
        
        public basePosition(url:string, callback:string):void
        {
            this.url = url;
            this.data = {};
            this.method = this.GET;
            Ajax.callback = callback;
            this.ajax();
        }
        
        static response(data:any):void
        {
            eval(Ajax.callback + '(data, Ajax.positionClick)');
        }

        private ajax():void
        {
            $.ajax({
                url: this.url,                 // указываем URL
            	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
                dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
            	data : this.data,	            //	даные, которые передаем
            	async : true,	            //	асинхронность запроса, по умолчанию true
            	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
            	contentType : "application/x-www-form-urlencoded",
            	type : this.method,            // GET либо POST

                success: function (data)
            	{	// вешаем свой обработчик на функцию success
            	    if(!data)
            	    {
            	        console.warn('false');
            	        return;
            	    }
                    Ajax.response(data);
                },

            	error: function (error)
            	{	// вешаем свой обработчик на функцию error
            		console.log(error);
            	},

            	beforeSend: function(){},	//	срабатывает перед отправкой запроса
            	complete: function(){}		//	срабатывает по окончанию запроса
            });
        }
    }
}