module UI
{
    import SceneGame = MyGame.SceneGame;
    export class UI
    {
        public showPlayerMenu(playerArr:Array<any>, activePlayer:any):void
        {
            let html = "";
            html += "<option value='"+ activePlayer.id +"' selected>"+ activePlayer.name +"</option>";
            for(let player of playerArr)
            {
                html += "<option value='"+ player.playerId +"'>"+ player.playerName +"</option>";
            }
            $('#myPlayer').text(activePlayer.name);
            $('#selectPlayer').html(html);
        }

        public onSelectPlayerOption():void
        {
            $('#selectPlayer').on('click', 'option', (e)=>
            {
                let playerId = $(e.target).attr('value');
                SceneGame.newActivePlayer(playerId);
            });
        }

        public positionMouse(x, y):void
        {
            $("#menu .mousePositionX").text(x);
            $("#menu .mousePositionY").text(y);
        }

        public positionBox(x, y, z):void
        {
            $("#menu .boxPositionX").text(x);
            $("#menu .boxPositionY").text(y);
            $("#menu .boxPositionZ").text(z);
        }

        public divideScalar(x, y, z):void
        {
            $("#menu .divideScalarX").text(x);
            $("#menu .divideScalarY").text(y);
            $("#menu .divideScalarZ").text(z);
        }

        public positionFloor(x, y, z):void
        {
            $("#menu .floorX").text(x);
            $("#menu .floorY").text(y);
            $("#menu .floorZ").text(z);
        }

        public positionMultiplyScalar(x, y, z):void
        {
            $("#menu .multiplyScalarX").text(x);
            $("#menu .multiplyScalarY").text(y);
            $("#menu .multiplyScalarZ").text(z);
        }

        public positionAddScalar(x, y, z):void
        {
            $("#menu .addScalarX").text(x);
            $("#menu .addScalarY").text(y);
            $("#menu .addScalarZ").text(z);
        }
    }
}