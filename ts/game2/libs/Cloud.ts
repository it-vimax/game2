import _GameColors = require('./GameColors'); import GameColors = _GameColors.GameColors;
export class Cloud
{
    public mesh:any;

    public constructor()
    {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "cloud";
        let geom = new THREE.BoxGeometry(20,20,20);
        let mat = new THREE.MeshPhongMaterial({
            color : GameColors.white,
        });

        let nBlocs = 3+Math.floor(Math.random()*3);
        for (let i=0; i<nBlocs; i++ ){
            let m = new THREE.Mesh(geom.clone(), mat);
            m.position.x = i*15;
            m.position.y = Math.random()*10;
            m.position.z = Math.random()*10;
            m.rotation.z = Math.random()*Math.PI*2;
            m.rotation.y = Math.random()*Math.PI*2;
            let s = 0.1 + Math.random()*0.9;
            m.scale.set(s,s,s);
            m.castShadow = true;
            m.receiveShadow = true;
            this.mesh.add(m);
        }
    }
}