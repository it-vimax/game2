import _GameColors = require('./GameColors'); import GameColors = _GameColors.GameColors;

export class Field
{
    public mesh:any;

    public constructor()
    {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "Field";
        let geom = new THREE.BoxGeometry(100, 100, 10);
        let mat = new THREE.MeshPhongMaterial({
            color : GameColors.blue
        });
        let m = new THREE.Mesh(geom, mat);
        m.position.x = 100;
        m.position.y = 10;
        m.position.z = 10;
        m.castShadow = true;
        m.receiveShadow = true;
        this.mesh.add(m);

        let geom2 = new THREE.BoxGeometry(100, 10, 10);
        let mat2 = new THREE.MeshPhongMaterial({
            color : GameColors.red
        });
        let m2 = new THREE.Mesh(geom2, mat2);
        m2.position.x = 0;
        m2.position.y = 0;
        m2.position.z = 0;
        m2.castShadow = true;
        m2.receiveShadow = true;
        this.mesh.add(m2);
    }
}