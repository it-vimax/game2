"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maks on 13.01.2018.
 */
var Server = (function () {
    function Server() {
    }
    Server.prototype.start = function () {
        this.io = require('socket.io')(6002);
        var Redis = require('ioredis');
        this.redis = new Redis();
        console.log('Server Start');
    };
    Server.prototype.pmessage = function () {
        var _this = this;
        this.redis.on('pmessage', function (pattern, channel, message) {
            message = JSON.parse(message);
            _this.io.emit(channel + ':' + message.event, message.data.message);
        });
    };
    return Server;
}());
exports.Server = Server;
