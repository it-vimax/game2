<?php
use app\assets\ControlDivAsset;
ControlDivAsset::register($this);
?>
<style>
    body {
        margin: 0px;
        overflow: hidden;
    }

    #overlay {
        position: absolute;
        top: 40%;
        left: 40%;
        width: 20%;
        height: 20%;
        background-color: #f00;
        padding: 3%;
        text-align: center;
        color: #fff;
        box-sizing: border-box;
    }
</style>
<div id="container"></div>

<!-- This div below stop the OrbitControls events, why? -->
<div id="overlay">
    I am a div with position:absolute
    <input type="text">
    <select name="a" id="a">
        <option value="a">a</option>
        <option value="a">a</option>
        <option value="a">a</option>
        <option value="a">a</option>
    </select>
</div>
