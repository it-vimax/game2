var container;
var camera, scene, renderer;
var uniforms, material, mesh;
var controls;
init();
animate();

function init() {
    container = document.getElementById('container');
    var aspect = window.innerWidth / window.innerHeight;
    camera = new THREE.PerspectiveCamera(45, aspect, 0.1, 1500);
    camera.position.set(1, 1, 1);
    scene = new THREE.Scene();
    renderer = new THREE.WebGLRenderer();
    container.appendChild(renderer.domElement);
    renderer.setSize(window.innerWidth, window.innerHeight);
    controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

    var geometry = new THREE.BoxGeometry(1, 1, 1);
    var material = new THREE.MeshBasicMaterial({
        color: 0x00ff00
    });
    var cube = new THREE.Mesh(geometry, material);

    scene.add(cube);
}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    renderer.render(scene, camera);
}