var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        MyGame.SceneGame.initSceneGame();
    };
    return Main;
}());
$(document).ready(function () {
    Main.main();
});
var ObjectScene;
(function (ObjectScene) {
    var BaseObj = (function () {
        function BaseObj() {
        }
        BaseObj.prototype.setGeometry = function (geometry) {
            this.geometry = geometry;
        };
        BaseObj.prototype.setMaterial = function (material) {
            this.material = material;
        };
        BaseObj.prototype.mesh = function () {
            this.obj = new THREE.Mesh(this.geometry, this.material);
        };
        return BaseObj;
    }());
    ObjectScene.BaseObj = BaseObj;
})(ObjectScene || (ObjectScene = {}));
var ObjectScene;
(function (ObjectScene) {
    var Cube = (function (_super) {
        __extends(Cube, _super);
        function Cube() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Cube;
    }(ObjectScene.BaseObj));
    ObjectScene.Cube = Cube;
})(ObjectScene || (ObjectScene = {}));
var ObjectScene;
(function (ObjectScene) {
    var Plane = (function (_super) {
        __extends(Plane, _super);
        function Plane() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Plane;
    }(ObjectScene.BaseObj));
    ObjectScene.Plane = Plane;
})(ObjectScene || (ObjectScene = {}));
var ObjectScene;
(function (ObjectScene) {
    var RedBox = (function (_super) {
        __extends(RedBox, _super);
        function RedBox() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return RedBox;
    }(ObjectScene.BaseObj));
    ObjectScene.RedBox = RedBox;
})(ObjectScene || (ObjectScene = {}));
var MyGame;
(function (MyGame) {
    var SceneGame = (function () {
        function SceneGame() {
        }
        SceneGame.initSceneGame = function () {
            SceneGame.ui = new UI.UI();
            SceneGame.init();
            SceneGame.animate();
        };
        SceneGame.onWindowResize = function () {
            SceneGame.camera.aspect = window.innerWidth / window.innerHeight;
            SceneGame.camera.updateProjectionMatrix();
            SceneGame.renderer.setSize(window.innerWidth, window.innerHeight);
        };
        SceneGame.onDocumentKeyDown = function (event) {
            switch (event.keyCode) {
                case 16:
                    SceneGame.isShiftDown = true;
                    break;
            }
        };
        SceneGame.onDocumentKeyUp = function (event) {
            switch (event.keyCode) {
                case 16:
                    SceneGame.isShiftDown = false;
                    break;
            }
        };
        SceneGame.onDocumentMouseDown = function (event) {
            SceneGame.mouse.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1);
            SceneGame.raycaster.setFromCamera(SceneGame.mouse, SceneGame.camera);
            var intersects = SceneGame.raycaster.intersectObjects(SceneGame.objects);
            if (intersects.length > 0) {
                var intersect = intersects[0];
                if (SceneGame.isShiftDown) {
                    if (intersect.object != SceneGame.plane.obj) {
                        SceneGame.scene.remove(intersect.object);
                        SceneGame.objects.splice(SceneGame.objects.indexOf(intersect.object), 1);
                    }
                }
                else {
                    SceneGame.cursorBox.obj.position.copy(intersect.point).add(intersect.face.normal);
                    var positionCube = {
                        positionX: Math.floor(SceneGame.cursorBox.obj.position.x / 50),
                        positionY: Math.floor(SceneGame.cursorBox.obj.position.y / 50),
                        positionZ: Math.floor(SceneGame.cursorBox.obj.position.z / 50),
                        userId: SceneGame.player1Id
                    };
                    SceneGame.ajax.get('/game-3/step', positionCube, 'MyGame.SceneGame.step', intersect);
                }
            }
        };
        SceneGame.setBasePosition = function (data) {
            var player;
            var otherPlayersServer = [];
            for (var _i = 0, _a = data.players; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.state == SceneGame.ACTIVE) {
                    SceneGame.player1Id = item.id;
                    player = item;
                }
                else {
                    otherPlayersServer.push(item);
                }
            }
            if (player !== undefined) {
                SceneGame.player.obj.position.x = player.position_x;
                SceneGame.player.obj.position.y = player.position_y;
                SceneGame.player.obj.position.z = player.position_z;
                SceneGame.player.obj.position.multiplyScalar(50).addScalar(25);
                var resFindElement = SceneGame.objects.some(function (item) {
                    return item.id == SceneGame.player.obj.id;
                });
                if (!resFindElement) {
                    SceneGame.scene.add(SceneGame.player.obj);
                    SceneGame.objects.push(SceneGame.player.obj);
                }
                console.log('Носая позиция кубика с id ' + SceneGame.player1Id + ' встановлена!');
            }
            else {
                console.log('не найдено обьекта с id ' + SceneGame.player1Id);
            }
            if (otherPlayersServer.length > 0) {
                for (var _b = 0, _c = SceneGame.otherPlayers; _b < _c.length; _b++) {
                    var fo = _c[_b];
                    if (fo.playerId == player.id) {
                        var findRemoveObj = SceneGame.scene.getObjectById(fo.objectId);
                        SceneGame.scene.remove(findRemoveObj);
                    }
                }
                SceneGame.otherPlayers = SceneGame.otherPlayers.filter(function (value) {
                    return value.playerId !== player.id;
                });
                otherPlayersServer.forEach(function (item, index, array) {
                    var otherPlayer = new ObjectScene.Cube();
                    otherPlayer.texturePath = "/img/game-3/box.png";
                    otherPlayer.setGeometry(new THREE.BoxGeometry(50, 50, 50));
                    otherPlayer.setMaterial(new THREE.MeshLambertMaterial({ color: 0xfeb74c, map: new THREE.TextureLoader().load(SceneGame.player.texturePath) }));
                    otherPlayer.mesh();
                    otherPlayer.obj.position.x = item.position_x;
                    otherPlayer.obj.position.y = item.position_y;
                    otherPlayer.obj.position.z = item.position_z;
                    otherPlayer.obj.position.multiplyScalar(50).addScalar(25);
                    SceneGame.scene.add(otherPlayer.obj);
                    SceneGame.otherPlayers.push({
                        playerId: item.id,
                        playerName: item.name,
                        objectId: otherPlayer.obj.id
                    });
                    SceneGame.ui.showPlayerMenu(SceneGame.otherPlayers, player);
                    SceneGame.ui.onSelectPlayerOption();
                    SceneGame.objects.push(otherPlayer.obj);
                });
            }
        };
        SceneGame.step = function (server, intersect) {
            if (!server.response.collision.success) {
                console.log('Можно идти');
                SceneGame.player.obj.position.copy(intersect.point).add(intersect.face.normal);
                SceneGame.player.obj.position.divideScalar(50).floor().multiplyScalar(50).addScalar(25);
                var resFindElement = SceneGame.objects.some(function (item) {
                    return item.id == SceneGame.player.obj.id;
                });
                if (!resFindElement) {
                    SceneGame.scene.add(SceneGame.player.obj);
                    SceneGame.objects.push(SceneGame.player.obj);
                }
            }
            else {
                console.log('Клетка занята');
            }
        };
        SceneGame.init2 = function () {
            var camera, scene, renderer;
            var uniforms, material, mesh;
            var controls;
            init();
            animate();
            function init() {
                var aspect = window.innerWidth / window.innerHeight;
                camera = new THREE.PerspectiveCamera(45, aspect, 0.1, 1500);
                camera.position.set(1, 1, 1);
                scene = new THREE.Scene();
                renderer = new THREE.WebGLRenderer();
                $('#game-field').append(renderer.domElement);
                renderer.setSize(window.innerWidth, window.innerHeight);
                var geometry = new THREE.BoxGeometry(1, 1, 1);
                var material = new THREE.MeshBasicMaterial({
                    color: 0x00ff00
                });
                var cube = new THREE.Mesh(geometry, material);
                scene.add(cube);
            }
            function animate() {
                requestAnimationFrame(animate);
                render();
            }
            function render() {
                renderer.render(scene, camera);
            }
        };
        SceneGame.init = function () {
            SceneGame.ajax = new Send.Ajax();
            var aspect = window.innerWidth / window.innerHeight;
            SceneGame.camera = new THREE.PerspectiveCamera(45, aspect, 1, 10000);
            SceneGame.camera.position.set(500, 800, 1300);
            SceneGame.camera.lookAt(new THREE.Vector3());
            SceneGame.scene = new THREE.Scene();
            SceneGame.scene.background = new THREE.Color(0xf0f0f0);
            SceneGame.redBox = new ObjectScene.RedBox();
            SceneGame.redBox.setGeometry(new THREE.BoxGeometry(50, 50, 50));
            SceneGame.redBox.setMaterial(new THREE.MeshBasicMaterial({ color: 0xff0000, opacity: 0.5, transparent: true }));
            SceneGame.redBox.mesh();
            SceneGame.scene.add(SceneGame.redBox.obj);
            SceneGame.player = new ObjectScene.Cube();
            SceneGame.player.texturePath = "/img/game-3/box.png";
            SceneGame.player.setGeometry(new THREE.BoxGeometry(50, 50, 50));
            SceneGame.player.setMaterial(new THREE.MeshLambertMaterial({ color: 0x00ff00, map: new THREE.TextureLoader().load(SceneGame.player.texturePath) }));
            SceneGame.player.mesh();
            SceneGame.scene.add(SceneGame.player.obj);
            SceneGame.objects.push(SceneGame.player.obj);
            SceneGame.cursorBox = new ObjectScene.Cube();
            SceneGame.cursorBox.texturePath = "/img/game-3/box.png";
            SceneGame.cursorBox.setGeometry(new THREE.BoxGeometry(50, 50, 50));
            SceneGame.cursorBox.setMaterial(new THREE.MeshLambertMaterial({ color: 0xff0000, map: new THREE.TextureLoader().load(SceneGame.player.texturePath) }));
            SceneGame.cursorBox.mesh();
            var gridHelper = new THREE.GridHelper(1000, 20);
            SceneGame.scene.add(gridHelper);
            SceneGame.raycaster = new THREE.Raycaster();
            SceneGame.mouse = new THREE.Vector2();
            SceneGame.plane = new ObjectScene.Plane();
            SceneGame.plane.setGeometry(new THREE.PlaneBufferGeometry(1000, 1000));
            SceneGame.plane.geometry.rotateX(-Math.PI / 2);
            SceneGame.plane.setMaterial(new THREE.MeshBasicMaterial({ visible: true }));
            SceneGame.plane.mesh();
            SceneGame.scene.add(SceneGame.plane.obj);
            SceneGame.objects.push(SceneGame.plane.obj);
            var ambientLight = new THREE.AmbientLight(0x606060);
            SceneGame.scene.add(ambientLight);
            var directionalLight = new THREE.DirectionalLight(0xffffff);
            directionalLight.position.set(1, 0.75, 0.5).normalize();
            SceneGame.scene.add(directionalLight);
            SceneGame.renderer = new THREE.WebGLRenderer({ antialias: true });
            SceneGame.renderer.setPixelRatio(window.devicePixelRatio);
            SceneGame.renderer.setSize(window.innerWidth, window.innerHeight);
            $('#game-field').append(SceneGame.renderer.domElement);
            SceneGame.controls = new THREE.OrbitControls(SceneGame.camera, SceneGame.renderer.domElement);
            SceneGame.ajax.basePosition('/game-3/base', 'MyGame.SceneGame.setBasePosition');
            document.addEventListener('mousemove', SceneGame.onDocumentMouseMove, false);
            document.addEventListener('mousedown', SceneGame.onDocumentMouseDown, false);
            document.addEventListener('keydown', SceneGame.onDocumentKeyDown, false);
            console.log('ok');
            document.addEventListener('keyup', SceneGame.onDocumentKeyUp, false);
            window.addEventListener('resize', SceneGame.onWindowResize, false);
        };
        SceneGame.animate = function () {
            requestAnimationFrame(SceneGame.animate);
            SceneGame.render();
        };
        SceneGame.newActivePlayer = function (playerId) {
            SceneGame.ajax.activePlayer('/game-3/active-player', playerId, 'MyGame.SceneGame.onActivePlayer');
        };
        SceneGame.onActivePlayer = function () {
            SceneGame.ajax.basePosition('/game-3/base', 'MyGame.SceneGame.setBasePosition');
            console.log('поменяли пользователя');
        };
        SceneGame.render = function () {
            SceneGame.renderer.render(SceneGame.scene, SceneGame.camera);
        };
        SceneGame.onDocumentMouseMove = function (event) {
            var mouseSetX = (event.clientX / window.innerWidth) * 2 - 1;
            var mouseSetY = -(event.clientY / window.innerHeight) * 2 + 1;
            SceneGame.ui.positionMouse(mouseSetX, mouseSetY);
            SceneGame.mouse.set(mouseSetX, mouseSetY);
            SceneGame.raycaster.setFromCamera(SceneGame.mouse, SceneGame.camera);
            var intersects = SceneGame.raycaster.intersectObjects(SceneGame.objects);
            if (intersects.length > 0) {
                var intersect = intersects[0];
                var posTest = SceneGame.redBox.obj.position.copy(intersect.point);
                var addTest = SceneGame.redBox.obj.position.add(intersect.face.normal);
                SceneGame.ui.positionBox(addTest.x, addTest.y, addTest.z);
                var divideScalarTest = SceneGame.redBox.obj.position.divideScalar(50);
                SceneGame.ui.divideScalar(divideScalarTest.x, divideScalarTest.y, divideScalarTest.z);
                var floorTest = SceneGame.redBox.obj.position.floor();
                SceneGame.ui.positionFloor(floorTest.x, floorTest.y, floorTest.z);
                var multiplyScalarTest = SceneGame.redBox.obj.position.multiplyScalar(50);
                SceneGame.ui.positionMultiplyScalar(multiplyScalarTest.x, multiplyScalarTest.y, multiplyScalarTest.z);
                var addScalarTest = SceneGame.redBox.obj.position.addScalar(25);
                SceneGame.ui.positionAddScalar(addScalarTest.x, addScalarTest.y, addScalarTest.z);
            }
        };
        return SceneGame;
    }());
    SceneGame.objects = [];
    SceneGame.otherPlayers = [];
    SceneGame.isShiftDown = false;
    SceneGame.ACTIVE = 1;
    MyGame.SceneGame = SceneGame;
})(MyGame || (MyGame = {}));
var Send;
(function (Send) {
    var Ajax = (function () {
        function Ajax() {
            this.GET = 'get';
            this.POST = 'post';
        }
        Ajax.prototype.activePlayer = function (url, palyerId, callback) {
            this.data = {
                playerId: palyerId
            };
            this.url = url;
            this.method = this.GET;
            Ajax.callback = callback;
            this.ajax();
        };
        Ajax.prototype.get = function (url, data, callback, positionClick) {
            this.url = url;
            this.data = data;
            this.method = this.GET;
            Ajax.callback = callback;
            Ajax.positionClick = positionClick;
            this.ajax();
        };
        Ajax.prototype.basePosition = function (url, callback) {
            this.url = url;
            this.data = {};
            this.method = this.GET;
            Ajax.callback = callback;
            this.ajax();
        };
        Ajax.response = function (data) {
            eval(Ajax.callback + '(data, Ajax.positionClick)');
        };
        Ajax.prototype.ajax = function () {
            $.ajax({
                url: this.url,
                dataType: "json",
                data: this.data,
                async: true,
                cache: true,
                contentType: "application/x-www-form-urlencoded",
                type: this.method,
                success: function (data) {
                    if (!data) {
                        console.warn('false');
                        return;
                    }
                    Ajax.response(data);
                },
                error: function (error) {
                    console.log(error);
                },
                beforeSend: function () { },
                complete: function () { }
            });
        };
        return Ajax;
    }());
    Send.Ajax = Ajax;
})(Send || (Send = {}));
var UI;
(function (UI_1) {
    var SceneGame = MyGame.SceneGame;
    var UI = (function () {
        function UI() {
        }
        UI.prototype.showPlayerMenu = function (playerArr, activePlayer) {
            var html = "";
            html += "<option value='" + activePlayer.id + "' selected>" + activePlayer.name + "</option>";
            for (var _i = 0, playerArr_1 = playerArr; _i < playerArr_1.length; _i++) {
                var player = playerArr_1[_i];
                html += "<option value='" + player.playerId + "'>" + player.playerName + "</option>";
            }
            $('#myPlayer').text(activePlayer.name);
            $('#selectPlayer').html(html);
        };
        UI.prototype.onSelectPlayerOption = function () {
            $('#selectPlayer').on('click', 'option', function (e) {
                var playerId = $(e.target).attr('value');
                SceneGame.newActivePlayer(playerId);
            });
        };
        UI.prototype.positionMouse = function (x, y) {
            $("#menu .mousePositionX").text(x);
            $("#menu .mousePositionY").text(y);
        };
        UI.prototype.positionBox = function (x, y, z) {
            $("#menu .boxPositionX").text(x);
            $("#menu .boxPositionY").text(y);
            $("#menu .boxPositionZ").text(z);
        };
        UI.prototype.divideScalar = function (x, y, z) {
            $("#menu .divideScalarX").text(x);
            $("#menu .divideScalarY").text(y);
            $("#menu .divideScalarZ").text(z);
        };
        UI.prototype.positionFloor = function (x, y, z) {
            $("#menu .floorX").text(x);
            $("#menu .floorY").text(y);
            $("#menu .floorZ").text(z);
        };
        UI.prototype.positionMultiplyScalar = function (x, y, z) {
            $("#menu .multiplyScalarX").text(x);
            $("#menu .multiplyScalarY").text(y);
            $("#menu .multiplyScalarZ").text(z);
        };
        UI.prototype.positionAddScalar = function (x, y, z) {
            $("#menu .addScalarX").text(x);
            $("#menu .addScalarY").text(y);
            $("#menu .addScalarZ").text(z);
        };
        return UI;
    }());
    UI_1.UI = UI;
})(UI || (UI = {}));
//# sourceMappingURL=game_3.js.map