"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maks on 13.01.2018.
 */
var Server = (function () {
    function Server(port) {
        this.port = port;
    }
    Server.prototype.start = function () {
        this.io = require('socket.io')(this.port);
        var Redis = require('ioredis');
        this.redis = new Redis();
        console.log('Server Start ' + this.port);
    };
    Server.prototype.psubscribe = function () {
        this.redis.psubscribe('*', function (error, count) {
            //отправка событий
        });
    };
    Server.prototype.pmessage = function () {
        var _this = this;
        this.redis.on('pmessage', function (pattern, channel, message) {
            message = JSON.parse(message);
            console.log(channel + ':' + message.event + '= ' + message.data.message);
            _this.io.emit(channel + ':' + message.event, message.data);
        });
        this.io.on('connection', function (socket) {
            console.log('new connect.');
            socket.on('getPositionPlain', function (e) {
                var position = e;
                socket.broadcast.emit('setPositionPlain', position);
                // setInterval(function(e)
                // {
                //     console.log(position);
                //     socket.broadcast.emit('setPositionPlain', position);
                // }, 10000);
            });
        });
    };
    return Server;
}());
exports.Server = Server;
//# sourceMappingURL=Server.js.map