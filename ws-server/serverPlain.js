var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').(server);
var log4js = require('log4js');
var logger = log4js.getLogger();
var port = 6001;

logger.debug('Script has been started...');

server.listen(port);

io.on('connection', (socket)=>
{
    var position = {
        x: 40,
        y: 55
    };
    socket.broadcast.emit('sendPosition', position);
    logger.info(position + ' connection to chat!');
});